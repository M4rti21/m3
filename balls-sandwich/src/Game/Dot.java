package Game;

import java.awt.Color;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

@SuppressWarnings("serial")
public class Dot extends PhysicBody {
	int time;

	public Dot(String name, int x1, int y1, int x2, int y2, double angle, Color color, Field f) {
		super(name, x1, y1, x2, y2, angle, color, f);
		this.circle = true;
		this.colorSprite = true;
		time = 0;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		
	}
	
	@Override
	public void update() {
		super.update();
		time++;
		if (time > 500) {
			this.delete();
		}
	}

}
