package Game;

import java.util.Random;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

@SuppressWarnings("serial")
public class Enemy extends PhysicBody implements Eatable {
	int size;
	Player target;
	Random r;
	int cnt;
	int spd;
	boolean middle;
	Sprite incolision;

	public Enemy(String name, int x1, int y1, int x2, int y2, double angle, String img, Field f, Player p) {
		super(name, x1, y1, x2, y2, angle, img, f);
		this.target = p;
		this.r = new Random();
		this.cnt = 0;
		this.spd = 2;
		this.circle = true;
		this.trigger = false;
		this.middle = false;
		this.incolision = null;
		this.size = 50;
		this.x2 = this.x1 + size;
		this.y2 = this.y1 + size;
	}

	public void moviment() {
		cnt++;
		if (cnt > 200) {
			cnt = 0;
		} else if (cnt < 100) {
			if (target.size < this.size) {
				if (this.x1 < target.x1) {
					if (this.y1 < target.y1) {
						this.setVelocity(spd, spd);
					} else {
						this.setVelocity(spd, -spd);
					}
				} else {
					if (this.y1 < target.y1) {
						this.setVelocity(-spd, spd);
					} else {
						this.setVelocity(-spd, -spd);
					}
				}
			} else {
				if (this.x1 < target.x1) {
					if (this.y1 < target.y1) {
						this.setVelocity(-spd, -spd);
					} else {
						this.setVelocity(-spd, spd);
					}
				} else {
					if (this.y1 < target.y1) {
						this.setVelocity(spd, -spd);
					} else {
						this.setVelocity(spd, spd);
					}
				}
			}
		} else if (cnt == 100) {
			this.setVelocity(r.nextInt(-spd, spd), r.nextInt(-spd, spd));
		}
	}

	@Override
	public void update() {
		super.update();
		moviment();
		if (middle) {
			this.Middle(this, incolision);
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Dot) {
			sprite.delete();
			this.size+=10;
			this.x2 = this.x1 + size;
			this.y2 = this.y1 + size;
		}
		if (sprite instanceof Enemy) {
			if (((Enemy) sprite).size < this.size - 20) {
				this.size += ((Enemy) sprite).size;
				this.x2 = this.x1 + size;
				this.y2 = this.y1 + size;
				sprite.delete();
			} else {
				((Enemy) sprite).trigger = true;
			}
		} else if (sprite instanceof Player) {
			if (((Player) sprite).size < this.size - 20) {
				this.size += ((Player) sprite).size/2;
				this.x2 = this.x1 + size;
				this.y2 = this.y1 + size;
				sprite.delete();
			} else {
				((Player) sprite).trigger = true;
			}
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		if (sprite instanceof Eatable) {
			this.middle = false;
			this.incolision = null;
		}
		this.trigger = false;
	}
}
