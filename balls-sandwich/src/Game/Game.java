package Game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Game {

	public static Field f = new Field();
	static Window w = new Window(f);
	static double cnt = 0;

	public static void main(String[] args) throws InterruptedException {
		Player p = new Player("Nerd", 300, 600, 600, 900, 0, "img/circle.png", f);
		ArrayList<Enemy> enemies = new ArrayList<Enemy>();
		createEnemies(enemies, p, 100);
		// p.colorSprite = true;
		f.lockScroll(p, w);
		boolean sortir = false;
		while (!sortir) {
			f.draw();
			f.setScrollx(f.getScrollx() - 1);
			createDots(p);
			Thread.sleep(15);
			input(p);
		}
	}

	public static void input(Player p) {
		if (w.getPressedKeys().contains('w')) {
			p.moviment(Input.AMUNT);
		}
		if (w.getPressedKeys().contains('a')) {
			p.moviment(Input.ESQUERRA);
		}
		if (w.getPressedKeys().contains('s')) {
			p.moviment(Input.AVALL);
		}
		if (w.getPressedKeys().contains('d')) {
			p.moviment(Input.DRETA);
		}
	}

	private static void createDots(Player p) {
		if (cnt > 10) {
			ArrayList<Sprite> dots = new ArrayList<Sprite>();
			Random r = new Random();
			int distance = 500;
			int x1 = r.nextInt((int) p.x1 - distance, (int) p.x2 + distance);
			int y1 = r.nextInt((int) p.y1 - distance, (int) p.y2 + distance);
			int x2 = x1 + 30;
			int y2 = y1 + 30;
			Dot dot = new Dot("Dot", x1, y1, x2, y2, 30,
					new Color(r.nextInt(0, 255), r.nextInt(0, 255), r.nextInt(0, 255)), f);
			dots.add(dot);
			cnt = 0;
		}
		cnt++;
	}

	private static void createEnemies(ArrayList<Enemy> arr, Player p, int quantity) {
		Random r = new Random();
		for (int i = 0; i < quantity; i++) {
			int x = r.nextInt(-2000, 2000);
			int y = r.nextInt(-2000, 2000);
			arr.add(new Enemy("Nerd", x, y, x+1, y+1, 0, "img/enemy.png", f, p));
		}
	}
}
