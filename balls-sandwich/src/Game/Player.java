package Game;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

@SuppressWarnings("serial")
public class Player extends PhysicBody implements Eatable {
	int size;
	boolean middle;
	Sprite incolision;

	public Player(String name, int x1, int y1, int x2, int y2, double angle, String string, Field f) {
		super(name, x1, y1, x2, y2, angle, string,f);
		this.circle = true;
		this.middle = false;
		this.incolision = null;
		this.size = 50;
		this.x2 = this.x1 + size;
		this.y2 = this.y1 + size;
	}

	public void moviment(Input in) {
		int speed = 5;
		boolean smth =false;
		if (in == Input.AMUNT) {
			this.setVelocity(this.velocity[0], -speed);
			smth=true;
		}
		if (in == Input.ESQUERRA) {
			this.setVelocity(-speed,this.velocity[1]);
			smth=true;

		}
		if (in == Input.AVALL) {
			this.setVelocity(this.velocity[0], speed);
			smth=true;


		}
		if (in == Input.DRETA) {
			this.setVelocity(speed,this.velocity[1]);
			smth=true;


		}
		
		if(!smth) {
			this.setVelocity(0, 0);

		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Dot) {
			sprite.delete();
			this.size++;
			this.x2 = this.x1 + size;
			this.y2 = this.y1 + size;
		}
		if (sprite instanceof Enemy) {
			if (((Enemy) sprite).size < this.size - 20) {
				this.size += ((Enemy) sprite).size/2;
				this.x2 = this.x1 + size;
				this.y2 = this.y1 + size;
				sprite.delete();
			} else {
				((Enemy) sprite).trigger = true;
			}
		}
//		if (sprite instanceof Enemy || sprite instanceof Player) {
//			this.middle = true;
//			this.incolision = sprite;
//		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		if (sprite instanceof Enemy) {
			this.middle = false;
			this.incolision = null;
		}
		this.trigger = false;
	}
	
	@Override
	public void update() {
		if (middle) {
			this.Middle(this, incolision);
		}
		super.update();
	}
}
