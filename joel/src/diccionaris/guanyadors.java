package diccionaris;

import java.util.Scanner;
import java.util.TreeMap;

public class guanyadors {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int times = sc.nextInt();
        int items;
        for (int i = 0; i < times; i++) {
            TreeMap<String, Integer> surtidos = new TreeMap<String, Integer>();
            items = sc.nextInt();
            sc.nextLine();
            for (int j = 0; j < items-1; j++) {
                String[] line = sc.nextLine().split("-");
                surtidos.put(line[1], surtidos.getOrDefault(line[1], 0) + 1);
            }
            String thing = sc.nextLine();
            System.out.println(surtidos.getOrDefault(thing, 0));
        }
        sc.close();
    }
}
