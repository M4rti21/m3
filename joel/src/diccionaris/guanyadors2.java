package diccionaris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;

public class guanyadors2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int times = sc.nextInt();
        for (int i = 0; i < times; i++) {
            TreeMap<String, Integer> surtidos = new TreeMap<String, Integer>();
            int items = sc.nextInt();
            sc.nextLine();
            for (int j = 0; j < items; j++) {
                String[] line = sc.nextLine().split("-");
                surtidos.put(line[1], surtidos.getOrDefault(line[1], 0) + 1);
            }
            ArrayList<String> surtidosKeys = new ArrayList<String>();
            for (Entry<String, Integer> surtido : surtidos.entrySet()) {
                surtidosKeys.add(surtido.getValue().toString() + '-' + surtido.getKey());
            }
            Collections.sort(surtidosKeys);
            Collections.reverse(surtidosKeys);
            for (String a : surtidosKeys) {
                String[] thin = a.split("-");
                System.out.println(thin[1] + "-" + thin[0]);
            }
        }
        sc.close();
    }
}
