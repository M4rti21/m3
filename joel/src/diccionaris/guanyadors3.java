package diccionaris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;

public class guanyadors3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int times = sc.nextInt();
        int items;
        for (int i = 0; i < times; i++) {
            TreeMap<String, Integer> surtidos = new TreeMap<String, Integer>();
            items = sc.nextInt();
            sc.nextLine();
            for (int j = 0; j < items; j++) {
                String[] line = sc.nextLine().split("-");
                surtidos.put(line[0], surtidos.getOrDefault(line[0], 0) + Integer.parseInt(line[1]));
            }
            double average = 0;
            int min = Integer.MAX_VALUE;
            int max = Integer.MIN_VALUE;
            String minName = "";
            String maxName = "";
            for (Map.Entry<String, Integer> serie : surtidos.entrySet()) {
                average += serie.getValue();
                if (serie.getValue() < min) {
                    min = serie.getValue();
                    minName = serie.getKey();
                }
                if (serie.getValue() > max) {
                    max = serie.getValue();
                    maxName = serie.getKey();
                }
            }
            System.out.println(String.format("%.2f", average/surtidos.size()) + " " + minName + " " + maxName);
        }
        sc.close();
    }
}
