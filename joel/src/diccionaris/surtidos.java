package diccionaris;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class surtidos {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int times = sc.nextInt();
        int items;
        String cookie;
        for (int i = 0; i < times; i++) {
            TreeMap<String, Integer> surtidos = new TreeMap<String, Integer>();
            items = sc.nextInt();
            sc.nextLine();
            for (int j = 0; j < items; j++) {
                cookie = sc.nextLine();
                surtidos.put(cookie, surtidos.getOrDefault(cookie, 0) + 1);
            }
            //System.out.println(surtidos);
            int strs = 0;
            ArrayList<String> surtidosKeys = new ArrayList<String>();
            for (Entry<String, Integer> surtido : surtidos.entrySet()) {
                surtidosKeys.add(surtido.getValue().toString() + '-' + surtido.getKey());
            }
            while (surtidosKeys.size() > 3) {
                surtidosKeys = fixArray(surtidosKeys);
                //System.out.println(surtidosKeys);
                int str = 0;
                for (String surtido : surtidosKeys) {
                    if (str < 3) {
                        //System.out.println(surtido);
                        int amount = Integer.parseInt(surtido.split("-")[0]) - 1;
                        //System.out.println(amount);
                        String thing = surtido.split("-")[1];
                        String newSurtido = amount + "-" + thing;
                        surtidosKeys.set(surtidosKeys.indexOf(surtido), newSurtido);
                        str += 1;
                    }
                }
                if (str == 3)
                    strs += 1;
            }
            System.out.println(strs);
        }
        sc.close();
    }

    public static ArrayList<String> fixArray(ArrayList<String> alist) {
        ArrayList<String> revArrayList = new ArrayList<String>();
        Collections.sort(alist);
        for (int i = alist.size() - 1; i >= 0; i--) {
            if (Integer.parseInt(alist.get(i).split("-")[0]) != 0) {
                 revArrayList.add(alist.get(i));
            }
               
        }
        return revArrayList;
    }
}
