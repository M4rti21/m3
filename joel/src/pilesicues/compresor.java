package pilesicues;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class compresor {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Queue<Character> text = new LinkedList<Character>();
        int times = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < times; i++) {
            String txt = sc.nextLine();
            char[] letters = txt.toCharArray();
            for (char c : letters) {
                text.add(c);
            }
            int count = 0;
            char old = text.peek();
            for (int j = 0; j < letters.length; j++) {
                char curr = text.poll();
                if (curr == old) {
                    count++;
                } else {
                    System.out.print(count +""+ old);
                    old = curr;
                    count = 1;
                }
            }
            System.out.print(count +""+ old);
            System.out.println();
        }
        sc.close();

    }

}
