package pilesicues;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class dependencies {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            int v = sc.nextInt();
            sc.nextLine();
            HashMap<String, List<String>> ps = new HashMap<String, List<String>>();
            for (int j = 0; j < v; j++) {
                String l = sc.nextLine();
                String[] ls = l.split("-");
                if (ls.length > 1) {
                    ps.put(ls[0], Arrays.asList(ls[1].split(",")));
                } else {
                    ps.put(ls[0], new ArrayList<String>());
                }
            }
            System.out.println(ps);
            Queue<String> process = new LinkedList<String>();
            while (process.size() < ps.size()) {
                for (Entry<String, List<String>> s : ps.entrySet()) {
                    boolean good = true;
                    if (!process.contains(s.getKey())) {
                        for (int j = 0; j < s.getValue().size(); j++) {
                            if (!process.contains(s.getValue().get(j))) {
                                good = false;
                            }
                        }
                        if (good) {
                            process.add(s.getKey());
                        }
                    }
                }
            }
            for (String p : process) {
                System.out.print(p + " ");
            }
            System.out.println();
        }
        sc.close();
    }
}
