package pilesicues;

import java.util.Scanner;
import java.util.Stack;

public class html {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Stack<String> html = new Stack<String>();
        int times = sc.nextInt();
        sc.nextLine();
        boolean isOk = true;
        for (int i = 0; i < times; i++) {
            String tag = sc.nextLine();
            if (tag.charAt(1) == '/') {
                isOk = html.pop().equals(tag.replaceFirst("/", ""));
            } else if (!tag.contains("/")) {
                html.add(tag);
                isOk = false;
            } else {
                isOk = false;
            }
        }
        if (isOk)
            System.out.println("Etiquetes ben tancades");
        else
            System.out.println("Etiquetes mal tancades");
        sc.close();
    }

}
          