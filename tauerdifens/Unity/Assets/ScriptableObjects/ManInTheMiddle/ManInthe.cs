using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ManInthe : ScriptableObject
{
    public List<GameObject> enemies;
}
