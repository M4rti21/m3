using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyCounter : MonoBehaviour
{
    public static int money;
    public static string txt;
    int oldMoney;
    void Start()
    {
        txt = "Money: ";
        money = 200;
        this.GetComponent<TextMeshProUGUI>().text = txt + money;
        oldMoney = money;
        StartCoroutine(Money());
    }
    IEnumerator Money()
    {
        while (true)
        {
            money += 10;
            this.GetComponent<TextMeshProUGUI>().text = txt+money;
            yield return new WaitForSeconds(5);
        }
    }

    private void Update()
    {
        if (money < 0)
        {
            money = 0;
        }
        if (money != oldMoney)
        {
            oldMoney = money;
            this.GetComponent<TextMeshProUGUI>().text = txt + money;
        }
    }
}
