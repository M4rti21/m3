using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class globusSpawner : MonoBehaviour
{
    public ManInthe man;
    public Sprite Blu;
    public Sprite Yellow;
    public Sprite Red;
    public List<Waypoint> waypoints;
    public GameObject globus;

    void Start()
    {
        StartCoroutine(generateBallon());
        man.enemies.Clear();
    }

    IEnumerator generateBallon()
    {
        while (true)
        {
            GameObject newBalloon = Instantiate(globus);
            newBalloon.GetComponent<Transform>().position = waypoints[0].waypoint;
            for (int i = 0; i < waypoints.Count; i++)
            {
                newBalloon.GetComponent<globusController>().waypoints.Add(waypoints[i]);
            }
            int random = Random.Range(0, 100);
            if (random < 10)
            {
                newBalloon.GetComponent<globusController>().hp = 60;
                newBalloon.GetComponent<globusController>().spd = 6;
                newBalloon.GetComponent<globusController>().dmg = 30;
                if (Blu != null)
                    newBalloon.GetComponent<SpriteRenderer>().sprite = Blu;

            }
            else if (random < 40)
            {
                newBalloon.GetComponent<globusController>().hp = 20;
                newBalloon.GetComponent<globusController>().spd = 4;
                newBalloon.GetComponent<globusController>().dmg = 20;
                if (Yellow != null)
                    newBalloon.GetComponent<SpriteRenderer>().sprite = Yellow;
            }
            else
            {
                newBalloon.GetComponent<globusController>().hp = 10;
                newBalloon.GetComponent<globusController>().spd = 2;
                newBalloon.GetComponent<globusController>().dmg = 10;
                if (Red != null)
                    newBalloon.GetComponent<SpriteRenderer>().sprite = Red;
            }
            man.enemies.Add(newBalloon.gameObject);
            yield return new WaitForSeconds(1);
        }
    }
}
