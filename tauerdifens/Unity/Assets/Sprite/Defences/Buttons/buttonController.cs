using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class buttonController : MonoBehaviour
{

    public GameObject torre;
    public Sprite rabano;
    public Sprite nabo;
    public Sprite dragon;

    private void OnMouseDown()
    {
        if (this.tag == "Rabano")
        {
            if (MoneyCounter.money - 100 >= 0)
            {
                GameObject newTower = Instantiate(torre);
                MoneyCounter.money -= 100;
                newTower.GetComponent<torreController>().atk = 10;
                newTower.GetComponent<torreController>().rng = 6;
                newTower.GetComponent<torreController>().frt = 1;
                if (rabano != null)
                    newTower.GetComponent<SpriteRenderer>().sprite = rabano;
            }
        }
        else if (this.tag == "Nabo")
        {
            if (MoneyCounter.money - 200 >= 0)
            {
                GameObject newTower = Instantiate(torre);
                MoneyCounter.money -= 200;
                newTower.GetComponent<torreController>().atk = 20;
                newTower.GetComponent<torreController>().rng = 10;
                newTower.GetComponent<torreController>().frt = 1;
                if (nabo != null)
                    newTower.GetComponent<SpriteRenderer>().sprite = nabo;
            }
        }
        else if (this.tag == "Dragonfruit")
        {
            if (MoneyCounter.money - 500 >= 0)
            {
                GameObject newTower = Instantiate(torre);
                MoneyCounter.money -= 500;
                newTower.GetComponent<torreController>().atk = 50;
                newTower.GetComponent<torreController>().rng = 20;
                newTower.GetComponent<torreController>().frt = 1;
                if (dragon != null)
                    newTower.GetComponent<SpriteRenderer>().sprite = dragon;
            }
        }
    }

}
