using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class torreController : MonoBehaviour
{
    Vector3 mousePosition;
    Vector2 mousePosition2;
    Boolean followMouse = true;
    public int atk;
    public int rng;
    public int frt;
    public GameObject proyectilPrefab;
    public ManInthe man;

    private void Start()
    {
        StartCoroutine(shoot());
    }

    IEnumerator shoot()
    {
        while (true)
        {
            if (!followMouse && man.enemies.Count > 0)
            {
                for (int i = 0; i < man.enemies.Count; i++)
                {
                    if (Vector2.Distance(this.transform.position, this.man.enemies[i].transform.position) < rng)
                    {
                        GameObject nuevoProyectil = Instantiate(proyectilPrefab);
                        nuevoProyectil.transform.position = this.transform.position;
                        Vector2 direccion = this.man.enemies[i].transform.position - transform.position;
                        direccion.Normalize();
                        nuevoProyectil.GetComponent<Rigidbody2D>().AddForce(direccion * 20f, ForceMode2D.Impulse);
                        nuevoProyectil.GetComponent<ProyectilController>().atk = this.atk;
                        i = man.enemies.Count;
                    }
                }

            }
            yield return new WaitForSeconds(frt);
        }
        
    }

    void Update()
    {
        if (followMouse)
        {
            mousePosition = Input.mousePosition;
            mousePosition2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.GetComponent<Transform>().position = new Vector3(mousePosition2.x, mousePosition2.y, 1); ;
        }
    }

    void OnMouseDown()
    {
        if (gameObject.tag == "Tower")
        {
            followMouse = false;
        }
    }

}
