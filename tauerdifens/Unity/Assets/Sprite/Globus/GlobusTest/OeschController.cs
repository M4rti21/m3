using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class OeschController : MonoBehaviour
{
    float moveSpeed = 3f;
    public Transform[] wayPoints;
    // saber a quin waypoint s'ha d'anar
    int waypointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = wayPoints[waypointIndex].transform.position; 
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        if (transform.position.y > 5.50f)
        {
            Final();
        }
    }

    void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, wayPoints[waypointIndex].transform.position, moveSpeed * Time.deltaTime);

        if (transform.position == wayPoints[waypointIndex].transform.position)
        {
            waypointIndex++;
        }

        if (waypointIndex == wayPoints.Length)
        {
            waypointIndex = 0;
        }
    }

    void Final()
    {
        Destroy(this.gameObject);
    }

}
