using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class globusController : MonoBehaviour
{
    public int hp;
    public int spd;
    public int dmg;
    public List<Waypoint> waypoints;
    private Waypoint target;
    public ManInthe man;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Projectil")
        {
            Destroy(collision.gameObject);
            this.hp -= collision.GetComponent<ProyectilController>().atk;
            if (this.hp < 0)
            {
                MoneyCounter.money += dmg;
                Morirse();
            }
        }
    }

    void Start()
    {
        target = waypoints[0];
        Vector3 direccio = (target.waypoint - this.transform.position);
        direccio.Normalize();
        this.GetComponent<Rigidbody2D>().velocity = direccio * spd;
    }

    void Update()
    {
        if ((this.target.waypoint - this.transform.position).magnitude < 0.1)
        {
            this.waypoints.Remove(target);

            if (waypoints.Count > 0)
            {
                target = waypoints[0];
                Vector3 direccio = (target.waypoint - this.transform.position);
                direccio.Normalize();
                this.GetComponent<Rigidbody2D>().velocity = direccio * spd;
            }
            else
            {
                Morirse();
            }
        }
    }

    void Morirse()
    {
        this.man.enemies.Remove(this.gameObject);
        Destroy(this.gameObject);
    }
}
