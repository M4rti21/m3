using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.RuleTile.TilingRuleOutput;

[CreateAssetMenu]
public class globus : ScriptableObject
{
    public Sprite img;
    public int hp;
    public int spd;
    public int dmg;
}
